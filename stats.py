from sqlalchemy import Column, Float, Integer, String, DateTime
from base import Base


class Stats(Base):
    """ Stats """

    __tablename__ = "stats"

    id = Column(Integer, primary_key=True)
    num_cm_readings = Column(Integer, nullable=True)
    max_cm_reading = Column(Integer, nullable=True)
    num_nd_readings = Column(Integer, nullable=True)
    max_nd_reading = Column(Integer, nullable=True)
    carbon_monoxide_last_updated = Column(DateTime, nullable=False)
    nitrogen_dioxide_last_updated = Column(DateTime, nullable=False)
    last_updated = Column(DateTime, nullable=False)

    def __init__(self, num_cm_readings, max_cm_reading, num_nd_readings,  max_nd_reading, carbon_monoxide_last_updated, nitrogen_dioxide_last_updated, last_updated):
        """ Initializes an income reading """
        self.num_cm_readings = num_cm_readings
        self.max_cm_reading = max_cm_reading
        self.num_nd_readings = num_nd_readings
        self.max_nd_reading = max_nd_reading
        self.carbon_monoxide_last_updated = carbon_monoxide_last_updated
        self.nitrogen_dioxide_last_updated = nitrogen_dioxide_last_updated
        self.last_updated = last_updated

    def to_dict(self):
        """ Dictionary Representation of an income reading """
        dict = {}
        dict['num_cm_readings'] = self.num_cm_readings
        dict['max_cm_reading'] = self.max_cm_reading
        dict['num_nd_readings'] = self.num_nd_readings
        dict['max_nd_reading'] = self.max_nd_reading
        dict['carbon_monoxide_last_updated'] = self.carbon_monoxide_last_updated.strftime("%Y-%m-%d %H:%M:%S.%f")
        dict['nitrogen_dioxide_last_updated'] = self.nitrogen_dioxide_last_updated.strftime("%Y-%m-%d %H:%M:%S.%f")
        dict['last_updated'] = self.last_updated.strftime("%Y-%m-%d %H:%M:%S.%f")

        return dict
