import sqlite3

conn = sqlite3.connect('stats.sqlite')

c = conn.cursor()
c.execute('''
            CREATE TABLE stats
            (id INTEGER PRIMARY KEY ASC, 
            num_cm_readings INTEGER NOT NULL,
            max_cm_reading INTEGER NOT NULL,
            num_nd_readings INTEGER NOT NULL,
            max_nd_reading INTEGER NOT NULL,
            carbon_monoxide_last_updated VARCHAR(100) NOT NULL,
            nitrogen_dioxide_last_updated VARCHAR(100) NOT NULL,
            last_updated VARCHAR(100) NOT NULL)
            ''')

conn.commit()
conn.close()
