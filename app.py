import datetime
import logging.config
import uuid
import os
import sqlite3
import connexion
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import yaml
from apscheduler.schedulers.background import BackgroundScheduler
import requests
from flask_cors import CORS
from base import Base
from stats import Stats


def create_database(path):
    conn = sqlite3.connect(path)

    c = conn.cursor()
    c.execute('''
                CREATE TABLE stats
                (id INTEGER PRIMARY KEY ASC, 
                num_cm_readings INTEGER NOT NULL,
                max_cm_reading INTEGER NOT NULL,
                num_nd_readings INTEGER NOT NULL,
                max_nd_reading INTEGER NOT NULL,
                carbon_monoxide_last_updated VARCHAR(100) NOT NULL,
                nitrogen_dioxide_last_updated VARCHAR(100) NOT NULL,
                last_updated VARCHAR(100) NOT NULL)
                ''')

    conn.commit()
    conn.close()


path = '/data/data.sqlite'
isExist = os.path.exists(path)
if isExist == True:
    print("data.sqlite already Exists")
else:
    create_database(path)

if "TARGET_ENV" in os.environ and os.environ["TARGET_ENV"] == "test":
    print("In Test Environment")
    app_conf_file = "/config/app_conf.yml"
    log_conf_file = "/config/log_conf.yml"
else:
    print("In Dev Environment")
    app_conf_file = "app_conf.yml"
    log_conf_file = "log_conf.yml"

with open(app_conf_file, 'r') as f:
    app_config = yaml.safe_load(f.read())

# External Logging Configuration
with open(log_conf_file, 'r') as f:
    log_config = yaml.safe_load(f.read())
    logging.config.dictConfig(log_config)
logger = logging.getLogger('basicLogger')

logger.info("App Conf File: %s" % app_conf_file)
logger.info("Log Conf File: %s" % log_conf_file)

# DB_ENGINE = create_engine(f"sqlite:///{path}")
DB_ENGINE = create_engine("sqlite:///%s" % app_config["datastore"]["filename"])
Base.metadata.bind = DB_ENGINE
DB_SESSION = sessionmaker(bind=DB_ENGINE)


def get_health():
    """ gets the health of each service and return 200 """
    logger.info("HEALTH CHECK - Return 200")
    dict = {"message": "running"}
    return dict, 200


def get_stats():
    """ Get proccessed result """
    trace_id = str(uuid.uuid4())
    logger.info(f"Start Getting Statistics (ID: {trace_id})")
    session = DB_SESSION()
    record = session.query(Stats).order_by(Stats.last_updated.desc())
    session.close()

    if record == None:
        logger.error(f"Statistics do not exist (ID: {trace_id})")
        return {"message": "Statistics do not exist"}, 404

    result = record[0].to_dict()
    logger.debug(f"Statistics {result} (ID: {trace_id})")
    logger.info(f"Complete Getting Statistics (ID: {trace_id})")

    return result, 200


def populate_stats():
    """ Periodically update stats """
    logger.info("-----------TESTING-------------------")
    trace_id = str(uuid.uuid4())
    logger.info(f"Start Periodic Processing (ID: {trace_id})")
    # current_timestamp = datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f")
    # headers = {"content-type": "application/json"}

    stats = get_latest_processing_stats()

    cm_latest_date = stats["carbon_monoxide_last_updated"]
    nd_latest_date = stats["nitrogen_dioxide_last_updated"]

    response_carbon_monoxide = requests.get(app_config["readings/carbon-monoxide"]['url'] + cm_latest_date)
    response_nitrogen_dioxide = requests.get(app_config["readings/nitrogen_dioxide"]['url'] + nd_latest_date)

    list1 = response_carbon_monoxide.json()
    list2 = response_nitrogen_dioxide.json()

    if response_carbon_monoxide.status_code == 200 and response_nitrogen_dioxide.status_code == 200:
        count_cb_readings = len(response_carbon_monoxide.json())
        count_nd_readings = len(response_nitrogen_dioxide.json())

        if count_cb_readings > 0:
            stats["num_cm_readings"] = len(response_carbon_monoxide.json())
            for record in response_carbon_monoxide.json():
                if cm_latest_date < record['date_created']:
                    cm_latest_date = record['date_created']

            for i in list1:
                # logger.debug(f"new event with a trace id of {i['trace_id']}")
                if i["carbon_monoxide_level"] > stats["max_cm_reading"]:
                    stats["max_cm_reading"] = i["carbon_monoxide_level"]

        if count_nd_readings > 0:
            stats["num_nd_readings"] = len(response_nitrogen_dioxide.json())
            for record in response_nitrogen_dioxide.json():
                if nd_latest_date < record['date_created']:
                    nd_latest_date = record['date_created']

            for i in list2:
                # logger.debug(f"new event with a trace id of {i['trace_id']}")
                if i["nitrogen_dioxide_level"] > stats["max_nd_reading"]:
                    stats["max_nd_reading"] = i["nitrogen_dioxide_level"]

        last_updated = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f")

        if count_cb_readings == 0 and count_nd_readings == 0:
            logger.info(f"No Data Updated (ID: {trace_id})")
        else:
            new_stats = Stats(stats["num_cm_readings"],
                              stats["max_cm_reading"],
                              stats["num_nd_readings"],
                              stats["max_nd_reading"],
                              datetime.datetime.strptime(cm_latest_date, "%Y-%m-%d %H:%M:%S.%f"),
                              datetime.datetime.strptime(nd_latest_date, "%Y-%m-%d %H:%M:%S.%f"),
                              # datetime.datetime.strptime(stats["last_updated"], "%Y-%m-%dT%H:%M:%S"),
                              # datetime.datetime.strptime(last_updated, "%Y-%m-%d %H:%M:%S.%f"))
                              datetime.datetime.now())

            session = DB_SESSION()
            session.add(new_stats)
            logger.debug(
                f"Updated statistics values : num_cm_readings = {stats['num_cm_readings']}, max_cm_reading = {stats['max_cm_reading']}, num_nd_readings = {stats['num_nd_readings']}, max_nd_reading = {stats['max_nd_reading']}")
            logger.info(f"Finish Periodic Processing Successfully (ID: {trace_id})")

            session.commit()
            session.close()
            # return list

            # logger.info(f"Finish Periodic Processing Successfully (ID: {trace_id})")
    else:
        logger.info(f"Periodic Processing Not Working (ID: {trace_id})")


def get_latest_processing_stats():
    """ Get the latest stats object, or None if there isn't one """
    session = DB_SESSION()
    results = session.query(Stats).order_by(Stats.last_updated.desc())
    session.close()

    if len(results.all()) > 0:
        return results.first().to_dict()

    return {"num_cm_readings": 0,
            "max_cm_reading": 0,
            "num_nd_readings": 0,
            "max_nd_reading": 0,
            "carbon_monoxide_last_updated": "2016-11-15 20:10:30.495612",
            "nitrogen_dioxide_last_updated": "2016-11-15 20:10:30.495612",
            "last_updated": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f")}


def init_scheduler():
    sched = BackgroundScheduler(daemon=True)
    sched.add_job(populate_stats, 'interval', seconds=app_config['scheduler']['period_sec'])
    sched.start()


app = connexion.FlaskApp(__name__, specification_dir='')
if "TARGET_ENV" not in os.environ or os.environ["TARGET_ENV"] != "test":
    CORS(app.app)
    app.app.config['CORS_HEADERS'] = 'Content-Type'

#CORS(app.app)
#app.app.config['CORS_HEADERS'] = 'Content-Type'
app.add_api("openapi.yaml", base_path="/processing", strict_validation=True, validate_responses=True)

if __name__ == "__main__":
    # run our standalone gevent server
    init_scheduler()
    app.run(port=8100, use_reloader=False)
