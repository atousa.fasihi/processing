from sqlalchemy import Column, Integer, String, DateTime
from base import Base
import datetime


class CarbonMonoxide(Base):
    """ carbon monoxide """

    __tablename__ = "carbon_monoxide"

    id = Column(Integer, primary_key=True)
    carbon_monoxide_level = Column(Integer, nullable=False)
    device_id = Column(String(250), nullable=False)
    temperature = Column(Integer, nullable=False)
    timestamp = Column(String(100), nullable=False)
    date_created = Column(DateTime, nullable=False)
    trace_id = Column(String(250), nullable=False)

    def __init__(self, carbon_monoxide_level, device_id, temperature, timestamp, trace_id):
        """ Initializes a carbon monoxide reading """
        self.carbon_monoxide_level = carbon_monoxide_level
        self.device_id = device_id
        self.temperature = temperature
        self.timestamp = timestamp
        self.date_created = datetime.datetime.now() # Sets the date/time record is created
        self.trace_id = trace_id

    def to_dict(self):
        """ Dictionary Representation of a carbon monoxide level reading """
        dict = {}
        dict['id'] = self.id
        dict['carbon_monoxide_level'] = self.carbon_monoxide_level
        dict['device_id'] = self.device_id
        dict['temperature'] = self.temperature
        dict['timestamp'] = self.timestamp
        dict['date_created'] = self.date_created
        dict['trace_id'] = self.trace_id

        return dict
